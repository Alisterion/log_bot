import {TelegramBotDriver} from './driver/telegram.bot.driver';
import * as config from 'config';
import {Bot} from './models/bot';

export class BotFather {
    telegramMainBot: TelegramBotDriver;
    bots: any = {};

    constructor() {
        //create main bot
        this.telegramMainBot = new TelegramBotDriver(config.get('telegram.mainBotToken'), 'father', true);
    }


    public async initBot(projectId: string, type: string = 'telegram', token: string) {
        this.bots[projectId] = new TelegramBotDriver(token, projectId);
        console.log(`Inited bot for project ${projectId}`);
    }

    public async addNewBot(projectId: string, type: string, token: string) {

        //add new bot in database
        const sub = new Bot({project: projectId, type, token});
        await sub.save();

        this.initBot(projectId, type, token);

    }

    public async init() {

        const bots = await Bot.find();

        bots.map((x: any) => {
            this.initBot(x.project, x.type, x.token)
        })
        //init all bots in system
    }


}