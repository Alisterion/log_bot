import * as mongoose from 'mongoose';

export const subscribedSchema = new mongoose.Schema({
    project: {type: String, required: true},
    chatId: {type: String, required: true},
});

export const Subscribed = mongoose.model('Subscribed', subscribedSchema);