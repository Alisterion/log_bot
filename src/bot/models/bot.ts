import * as mongoose from 'mongoose';

export const botSchema = new mongoose.Schema({
    project: {type: String, required: true},
    token: {type: String, unique: true, required: true},
    type: {type: String, required: true},
});

export const Bot = mongoose.model('Bot', botSchema);