import * as TelegramBot from 'node-telegram-bot-api';
import {Subscribed} from '../models/subscribed';
import {Bot} from '../models/bot';
import {APP} from '../../../index';

export class TelegramBotDriver {
    public readonly bot: any;
    public isMain: boolean = false;
    public projectId: string;

    constructor(token: string, projectId: string, isMain: boolean = false) {
        this.bot = new TelegramBot(token, {polling: true});
        //need separate to different object main bot and simple bot
        this.isMain = isMain;
        this.projectId = projectId;

        this.bot.onText(/\/start/, this.start.bind(this));
        this.bot.onText(/\/hello/, this.hello.bind(this));
        this.bot.onText(/\/add_bot (.+) (.+)/, this.addBot.bind(this));
        this.bot.onText(/\/list/, this.list.bind(this));
    }

    public async hello(msg: any, match: any) {
        const resp = `Hello I'm a BOT.`;

        const chatId = msg.chat.id;
        this.bot.sendMessage(chatId, resp);
    }

    public async list(msg: any, match: any) {
        if (!this.isMain) {
            return;
        }
        const res = await Bot.find();
        const chatId = msg.chat.id;

        this.bot.sendMessage(chatId, res.join('\n'));
    }


    public async start(msg: any, match: any) {
        const chatId = msg.chat.id;
        let resp = 'Already subscribed';

        const res = await Subscribed.findOne({project: this.projectId, chatId});

        if (!res) {
            const sub = new Subscribed({chatId, project: this.projectId});
            await sub.save();
            resp = 'Subscribed';
        }
        this.bot.sendMessage(chatId, resp);
    }

    public async addBot(msg: any, match: any) {
        if (!this.isMain) {
            return;
        }
        const chatId = msg.chat.id;

        const project = match[1];
        const token = match[2];


        await APP.botFather.addNewBot(project, 'telegram', token);
        const resp = `Bot successfully saved`;
        this.bot.sendMessage(chatId, resp);
    }

    public async send(msg: string) {
        const subscribed = await Subscribed.find({project: this.projectId});

        subscribed.map((x: any) => {
            try {
                this.bot.sendMessage(x.chatId, msg);
            } catch (e) {
                console.log(e.message);
            }
        });
    }

}
