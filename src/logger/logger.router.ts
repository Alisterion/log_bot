import * as express from 'express';
import * as validation from 'express-validation';
import {RouterInterface} from '../type';
import {LoggerController} from './logger.controller';
import {LogValidator} from './logger.validation';

export class LoggerRouter implements RouterInterface {
    public path: string = '/logger';
    public readonly controller: LoggerController;

    constructor() {
        this.controller = new LoggerController();
    }

    public create(): express.Router {
        const router = express.Router();
        router.post('/',
            validation(LogValidator),
            this.controller.create.bind(this.controller),
        );
        return router;
    }
}
