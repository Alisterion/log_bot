import {NextFunction, Request, Response} from 'express';
import * as HTTPStatus from 'http-status';
import {APP} from "../../index";

export class LoggerController {

    public async create(req: Request, res: Response, next: NextFunction) {
        const message = req.body.message;
        const project = req.body.project;

        await APP.botFather.telegramMainBot.send(message);
        await APP.botFather.bots[project].send(message);


        res.status(HTTPStatus.CREATED).json({});
    }
}