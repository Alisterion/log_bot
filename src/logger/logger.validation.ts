import * as Joi from 'joi';

export const LogValidator = {
    body: {
        project: Joi.string().required(),
        message: Joi.string().required()
    },
};
