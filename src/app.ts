import * as bluebird from 'bluebird';
import * as bodyParser from 'body-parser';
import * as config from 'config';
import * as cors from 'cors';

import * as express from 'express';
import {createServer, Server} from 'http';
import * as moment from 'moment';
import * as mongoose from 'mongoose';
import * as winston from 'winston';

import validation = require('express-validation');
import expressWinston = require('express-winston');
import {BotFather} from './bot';
import {LoggerRouter} from './logger/logger.router';

require('express-async-errors');

export class APPServer {
    public static readonly PORT: number = 3000;
    public readonly botFather: BotFather = new BotFather();
    private app: express.Application;
    private server: Server;
    private port: string | number;
    private baseURL = '/api';


    public getApp(): express.Application {
        return this.app;
    }

    public run() {
        this.createApp();
        this.config();
        this.initDb();
        this.createServer();
        this.createLogger();
        this.initRouters();
        this.listen();
        this.errorCatcher();

        this.botFather.init();

        winston.info(`Run server on ${this.app.settings.env.toUpperCase()} environment`);
    }

    private initDb(): void {
        const mongoUrl = config.get('db.url') as string;
        winston.info(`Connecting to ${mongoUrl}`);
        (mongoose).Promise = bluebird;
        mongoose.connect(mongoUrl, {
            promiseLibrary: bluebird,
            ...config.get('db.options'),
        }).then(
            () => {
                winston.info('MongoDB connection successfully.');
            },
        ).catch((err: any) => {
            winston.info('MongoDB connection error. Please make sure MongoDB is running. ' + err);
        });

    }

    private initRouters(): void {
        const loggerRouter = new LoggerRouter();
        this.app.use(`${this.baseURL}${loggerRouter.path}`, loggerRouter.create());
    }

    private createApp(): void {
        this.app = express();
    }

    private createLogger(): void {
        const winstonConfig = winston.config;
        this.app.use(expressWinston.logger({
            colorize: true,
            expressFormat: false,
            ignoreRoute: (req: any, res: any) => {
                return false;
            },
            meta: false,
            msg: (req: any, res: any) => {
                const user = req.user;
                return `[${res.statusCode}] <${moment(req._startTime)
                    .format('DD:MM:YYYY HH:mm:ss')}> (${res.responseTime}ms) ${user ? user.email : 'Anonymous' } ${req.method} ${req.url}`;
            },
            transports: [
                new winston.transports.Console({
                    colorize: true,
                    formatter: (options) => {
                        return winstonConfig.colorize(options.level, 'HTTP') + ': ' +
                            (options.message ? options.message : '') +
                            (options.meta
                            && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) : '');
                    },
                    json: false,
                    timestamp: () => {
                        return Date.now();
                    },
                }),
            ],
        }));

        this.app.use((req: any, res: any, next: any) => {
            req.log = new (winston.Logger)({
                transports: [
                    new (winston.transports.Console)({colorize: true}),
                ],
            });
            next();
        });
    }

    private createServer(): void {
        this.app.use(cors({}));
        this.app.use(bodyParser.json());

        this.server = createServer(this.app);
    }

    private config(): void {
        this.port = process.env.PORT || APPServer.PORT;
    }

    private errorCatcher(): void {
        this.app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
                if (err instanceof validation.ValidationError) {
                    return res.status(400).json(err);
                }
                winston.error(err);
                if (err.code >= 400 && err.code < 500) {
                    return res.status(400).json(err);
                }

                res.status(err.code && err.code <= 503 && err.code >= 500 ? err.code : 500).json({
                    message: err.message,
                    stack: err.stack,
                    status: err.status,
                });
            },
        );
    }

    private listen(): void {
        this.server.listen(this.port, () => {
            winston.info('Running server on port %s', this.port);
        });
    }

    private connectStatic() {
    }
}