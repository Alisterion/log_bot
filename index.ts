import {APPServer} from './src/app';

export const APP = new APPServer();

APP.run();
