FROM node:8.10.0
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json /usr/src/app/
COPY config /usr/src/app/config
COPY src /usr/src/app/src
COPY gulpfile.js /usr/src/app
COPY index.ts /usr/src/app
COPY process.yml /usr/src/app
COPY tsconfig.json /usr/src/app
COPY tslint.json /usr/src/app

RUN npm install
RUN npm install -g pm2
RUN npm run build
EXPOSE 4000
CMD ["pm2-runtime", "start" , "./process.yml"]
